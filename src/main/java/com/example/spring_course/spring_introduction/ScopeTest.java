package com.example.spring_course.spring_introduction;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeTest {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContext3.xml");

        Dog mydog = context.getBean("dogBean", Dog.class);
        mydog.say();

//        Dog yourDog = context.getBean("dogBean", Dog.class);
//
//        System.out.println("Peremenue sulaytca na odin i totje value " + (mydog == yourDog));
//        System.out.println(mydog);
//        System.out.println(yourDog);

        context.close();

    }
}
