package com.example.spring_course.spring_introduction;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test2 {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");
        Pet pet = context.getBean("myPet",Pet.class);
        pet.say();

        context.close();
    }
}
