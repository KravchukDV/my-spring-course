package com.example.spring_course.aop;

import org.springframework.context.annotation.Configuration;

@Configuration("libraryBean")
public class Library {
    public void getBook (){
        System.out.println("We get a book.");
    }
}
